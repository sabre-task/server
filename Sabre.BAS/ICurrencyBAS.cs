﻿using Sabre.Common;
using Sabre.DTO;
using Sabre.External.WebServices;
using System;
using System.Threading.Tasks;

namespace Sabre.BAS
{
    public interface ICurrencyBAS
    {
        Task<ServiceResult<ApiException, CurrencySymbolCollectionDto<CurrencySymbolDto>>> GetSymbolsAsync();
        Task<ServiceResult<ApiException, CurrencyRateCollectionDto<CurrencyRateDto>>> GetRatesAsync(RateRequestDto requestDto);
    }
}
