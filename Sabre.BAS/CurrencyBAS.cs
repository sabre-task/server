﻿using Sabre.Common;
using Sabre.DTO;
using Sabre.External.WebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabre.BAS
{
    public sealed class CurrencyBAS: ICurrencyBAS
    {
        private ICurrencyService _currencyService;
        public CurrencyBAS(ICurrencyService currencyService)
        {
            this._currencyService = currencyService;
        }

        public async Task<ServiceResult<ApiException, CurrencyRateCollectionDto<CurrencyRateDto>>> GetRatesAsync(RateRequestDto requestDto)
        {
            ApiResponse<ApiException, CurrencyRateCollection> result = await this._currencyService.GetRatesAsync(requestDto.Code);
            if (result.Error != null)
            {
                return new ServiceResult<ApiException, CurrencyRateCollectionDto<CurrencyRateDto>>(result.Error, null);
            }
            var collectionDto = new CurrencyRateCollectionDto<CurrencyRateDto>();
            foreach (var item in result.Data.Get)
            {
                collectionDto.Add(new CurrencyRateDto(null, item.Code, item.Description));
            }
            return new ServiceResult<ApiException, CurrencyRateCollectionDto<CurrencyRateDto>>(null, collectionDto);
        }

        public async Task<ServiceResult<ApiException, CurrencySymbolCollectionDto<CurrencySymbolDto>>> GetSymbolsAsync()
        {
            ApiResponse<ApiException, CurrencySymbolCollection> result = await this._currencyService.GetSymbolsAsync();
            if(result.Error != null)
            {
                return new ServiceResult<ApiException, CurrencySymbolCollectionDto<CurrencySymbolDto>>(result.Error, null);
            }
            var collectionDto = new CurrencySymbolCollectionDto<CurrencySymbolDto>();
            foreach(var item in result.Data.Get)
            {
                collectionDto.Add(new CurrencySymbolDto(item.Code, item.Description));
            }
            return new ServiceResult<ApiException, CurrencySymbolCollectionDto<CurrencySymbolDto>>(null, collectionDto);
        }
    }
}
