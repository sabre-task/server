﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace Sabre.Core
{
    public class AppConfiguration
    {
        private static AppConfiguration instance = null;
        private readonly string _connectionString = string.Empty;
        public IConfigurationSection ConnectionStringSection { get; private set; }
        public IConfigurationSection ApplicationSettingsSection { get; private set; }
        public static AppConfiguration Get
        {
            get
            {
                if (instance == null)
                {
                    instance = new AppConfiguration();
                }
                return instance;
            }
        }

        public AppConfiguration()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);

            var root = configurationBuilder.Build();
            ConnectionStringSection = root.GetSection("ConnectionStrings");
            ApplicationSettingsSection = root.GetSection("ApplicationSettings");
        }
    }
}

