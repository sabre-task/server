using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sabre.BAS;
using Sabre.Common;
using Sabre.DTO;
using Sabre.External.WebServices;
using Sabre.Tests.Fakes;
using Sabre.WebAPI.Controllers;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Sabre.Tests
{
    public class CurrencyControllerTest
    {
        private readonly CurrencyController _controller;
        private readonly ICurrencyBAS _service;

        public CurrencyControllerTest()
        {
            _service = new CurrencyBASFake();
            _controller = new CurrencyController(_service);
        }

        [Fact]
        public async void Should_Return_Symbols_OK()
        {
            var okResult = _controller.GetSymbols();
            Assert.IsType<JsonResult>(okResult.Result);
            Assert.Equal(StatusCodes.Status200OK, (okResult.Result as JsonResult).StatusCode);
        }

        [Fact]
        public async void Should_Return_Rates_OK()
        {
            var okResult = _controller.GetRates("EUR");
            Assert.IsType<JsonResult>(okResult.Result);
            Assert.Equal(StatusCodes.Status200OK, (okResult.Result as JsonResult).StatusCode);
        }
    }
}
