﻿using Sabre.BAS;
using Sabre.Common;
using Sabre.DTO;
using Sabre.External.WebServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabre.Tests.Fakes
{
    public class CurrencyBASFake : ICurrencyBAS
    {
        public async Task<ServiceResult<ApiException, CurrencySymbolCollectionDto<CurrencySymbolDto>>> GetSymbolsAsync()
        {
            var collection = new CurrencySymbolCollectionDto<CurrencySymbolDto>();
            collection.Add(new CurrencySymbolDto("EUR", "Europe"));
            collection.Add(new CurrencySymbolDto("AED", "United Arab Emirates Dirham"));

            return await Task.FromResult(new ServiceResult<ApiException, CurrencySymbolCollectionDto<CurrencySymbolDto>>(null, collection));
        }

        public async Task<ServiceResult<ApiException, CurrencyRateCollectionDto<CurrencyRateDto>>> GetRatesAsync(RateRequestDto requestDto)
        {
            var collection = new CurrencyRateCollectionDto<CurrencyRateDto>();
            collection.Add(new CurrencyRateDto("EUR", "AED", "4.203092"));
            collection.Add(new CurrencyRateDto("EUR", "AFD", "104.64745"));

            return await Task.FromResult(new ServiceResult<ApiException, CurrencyRateCollectionDto<CurrencyRateDto>>(null, collection));
        }
    }
}
