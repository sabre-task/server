### Setup
1. Requires dotnetcore 5 framework
2. Run `dotnet restore` to Restore all packages
3. cd into `Sabre.WebAPI` project
4. Run `dotnet run`
5. Application starts listening on http://localhost:5000
6. The postman API collection is attached to root of the project
7. cd into `Sabre.Test` project
8. Run `dotnet test` to unit mocked unit test cases