﻿using Microsoft.AspNetCore.Mvc;
using Sabre.BAS;
using System;
using System.Collections.Generic;
using System.Linq;
using Sabre.DTO;
using System.Threading.Tasks;
using Sabre.WebApi;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Sabre.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyController : ControllerBase
    {
        private ICurrencyBAS _currencyBas;
        public CurrencyController(ICurrencyBAS currencyBAS)
        {
            this._currencyBas = currencyBAS;
        }
       
        [HttpGet("Symbols")]
        public async Task<IActionResult> GetSymbols()
        {
            var result = await this._currencyBas.GetSymbolsAsync();
            if(result.Error != null)
            {
                return HttpResponseUtil.Error(result.Error.Code, result.Error.Message);
            }
            return HttpResponseUtil.Ok(result.Data);
        }

        [HttpGet("Rates")]
        public async Task<IActionResult> GetRates([FromQuery] string code = "EUR")
        {
            var result = await this._currencyBas.GetRatesAsync(new RateRequestDto() { Code = code });
            if (result.Error != null)
            {
                return HttpResponseUtil.Error(result.Error.Code, result.Error.Message);
            }
            return HttpResponseUtil.Ok(result.Data);
        }

        //// GET api/<CurrencyController>/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/<CurrencyController>
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT api/<CurrencyController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/<CurrencyController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
