﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sabre.WebApi
{
    public static class HttpResponseUtil
    {
        public static IActionResult Ok(object data)
        {
            return new JsonResult(new { data = data, status = "success" })
            {
                StatusCode = StatusCodes.Status200OK
            }; ;
        }

        public static IActionResult NotOk(object data)
        {
            return new JsonResult(new { data = data, status = "error" })
            {
                StatusCode = StatusCodes.Status400BadRequest
            };
        }

        public static IActionResult Error(string code, string message)
        {
            return new JsonResult(new { code = code, message = message, status = "error" })
            {
                StatusCode = StatusCodes.Status500InternalServerError
            };
        }

        public static IActionResult Unauthorized()
        {
            return new JsonResult(new { })
            {
                StatusCode = StatusCodes.Status401Unauthorized
            }; ;
        }
    }
}
