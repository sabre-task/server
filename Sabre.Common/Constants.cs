﻿using System;

namespace Sabre.Common
{
    public abstract class Constants
    {
        public abstract class CurrencyService
        {
            public const string FAILED_TO_FETCH_SYMBOLS = "FAILED_TO_FETCH_SYMBOLS";
            public const string FAILED_TO_FETCH_RATES = "FAILED_TO_FETCH_RATES";
        }
    }
}
