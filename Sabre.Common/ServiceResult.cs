﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabre.Common
{
    public class ServiceResult<E, D>
    {
        public readonly E Error;
        public readonly D Data;
        public ServiceResult(E error, D data)
        {
            this.Error = error;
            this.Data = data;
        }
    }
}
