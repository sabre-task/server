﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabre.DTO
{
    public sealed class CurrencySymbolCollectionDto<CurrencySymbolDto> : List<CurrencySymbolDto>
    {
        public new void Add(CurrencySymbolDto item)
        {
            base.Add(item);
        }
    }

    public sealed class CurrencySymbolDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public CurrencySymbolDto(string code, string description)
        {
            this.Code = code;
            this.Description = description;
        }
    }
}
