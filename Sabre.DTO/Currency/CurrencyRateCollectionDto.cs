﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabre.DTO
{
    public sealed class CurrencyRateCollectionDto<CurrencyRateDto> : List<CurrencyRateDto>
    {
        public new void Add(CurrencyRateDto item)
        {
            base.Add(item);
        }
    }

    public sealed class CurrencyRateDto
    {
        public string Code { get; set; }
        public string Base { get; set; }
        public string Rate { get; set; }
        public CurrencyRateDto(string _base, string code, string rate)
        {
            this.Base = _base;
            this.Code = code;
            this.Rate = rate;
        }
    }
}
