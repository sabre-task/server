﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabre.DTO
{
    public class RateRequestDto
    {
        public string Code { get; set; }
    }
}
