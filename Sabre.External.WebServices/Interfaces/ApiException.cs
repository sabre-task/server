﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabre.External.WebServices
{
    public class ApiException
    {
        public readonly Exception ActualException;
        public readonly string Code;
        public readonly string Message;
        public ApiException(string code, string message = "Service Error", Exception actualException = null)
        {
            this.Code = code;
            this.Message = message;
            this.ActualException = actualException;
        }
    }
}
