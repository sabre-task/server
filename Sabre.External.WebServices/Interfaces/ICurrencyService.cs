﻿using System;
using System.Threading.Tasks;

namespace Sabre.External.WebServices
{
    public interface ICurrencyService
    {
        Task<ApiResponse<ApiException, CurrencyRateCollection>> GetRatesAsync(string symbol = "EUR");
        Task<ApiResponse<ApiException, CurrencySymbolCollection>> GetSymbolsAsync();
    }
}
