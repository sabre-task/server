﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabre.External.WebServices
{
    public class ApiResponse<E, D>
    {
        public readonly E Error;
        public readonly D Data;
        public ApiResponse(E error, D data)
        {
            this.Error = error;
            this.Data = data;
        }
    }
}
