﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabre.External.WebServices
{
    public class CurrencyRatesResponse: ApiBase
    {
        public Dictionary<string, string> Rates { get; set; }
        public Int64 Timestamp { get; set; }
        public string Base { get; set; }
    }
}
