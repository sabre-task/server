﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabre.External.WebServices
{
    internal class CurrencySymbolsResponse: ApiBase
    {
        public Dictionary<string, string> Symbols { get; set; }
    }
}
