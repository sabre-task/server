﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabre.External.WebServices
{
    public sealed class ApiErrorInfo
    {
        public string Code;
        public string Type;
        public ApiErrorInfo()
        {

        }
        public ApiErrorInfo(string code, string type)
        {
            this.Code = code;
            this.Type = type;
        }
    }
}
