﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabre.External.WebServices
{
    public class ApiBase
    {
        public string Success { get; set; } = "true";
        public ApiErrorInfo Error { get; set; }
        public bool IsSuccess
        {
            get
            {
                return Success.ToUpper() == "TRUE";
            }
        }
    }
}
