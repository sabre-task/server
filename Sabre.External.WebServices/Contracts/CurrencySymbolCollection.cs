﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabre.External.WebServices
{
    public sealed class CurrencySymbolCollection
    {
        private IList<CurrencySymbol> _collection;

        public CurrencySymbolCollection()
        {
            this._collection = new List<CurrencySymbol>();
        }

        public void AddItem(CurrencySymbol item)
        {
            this._collection.Add(item);
        }

        public IList<CurrencySymbol> Get
        {
            get
            {
                return this._collection;
            }
        }
    }
}
