﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabre.External.WebServices
{
    public sealed class CurrencySymbol
    {
        public readonly string Code;
        public readonly string Description;

        public CurrencySymbol(string code, string description)
        {
            this.Code = code;
            this.Description = description;
        }
    }
}
