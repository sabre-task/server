﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sabre.External.WebServices
{
    public sealed class CurrencyRateCollection
    {
        private IList<CurrencyRate> _collection;

        public CurrencyRateCollection()
        {
            this._collection = new List<CurrencyRate>();
        }

        public void AddItem(CurrencyRate item)
        {
            this._collection.Add(item);
        }

        public IList<CurrencyRate> Get
        {
            get
            {
                return this._collection;
            }
        }
    }
}
