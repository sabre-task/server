﻿using Sabre.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Authenticators;
using Sabre.External.WebServices;
using Sabre.Common;

namespace Sabre.External.WebServices
{
    public sealed class CurrencyService : ICurrencyService
    {
        private readonly string _host;
        private readonly string _token;
        private RestClient _webClient;

        public CurrencyService()
        {
            _host = AppConfiguration.Get.ApplicationSettingsSection.GetSection("CurrencyApiHost").Value;
            _token = AppConfiguration.Get.ApplicationSettingsSection.GetSection("CurrencyApiToken").Value;
            _webClient = new RestClient(_host);
        }

        public async Task<ApiResponse<ApiException, CurrencyRateCollection>> GetRatesAsync(string symbol = "EUR")
        {
            try
            {
                var request = new RestRequest("api/latest", DataFormat.Json);
                request.AddQueryParameter("access_key", this._token);
                request.AddQueryParameter("format", "1");
                request.AddQueryParameter("base", symbol);
                var response = await _webClient.GetAsync<CurrencyRatesResponse>(request);
                if (response.IsSuccess)
                {
                    var collection = new CurrencyRateCollection();
                    foreach (var kv in response.Rates)
                    {
                        collection.AddItem(new CurrencyRate(kv.Key, kv.Value));
                    }
                    var result = new ApiResponse<ApiException, CurrencyRateCollection>(null, collection);
                    return await Task.FromResult(result);
                }

                return await Task.FromResult(new ApiResponse<ApiException, CurrencyRateCollection>(new ApiException(Constants.CurrencyService.FAILED_TO_FETCH_RATES, null), null));
            }
            catch (Exception ex)
            {
                var result = new ApiResponse<ApiException, CurrencyRateCollection>(new ApiException(Constants.CurrencyService.FAILED_TO_FETCH_RATES, null, ex), null);
                return await Task.FromResult(result);
            }
        }

        public async Task<ApiResponse<ApiException, CurrencySymbolCollection>> GetSymbolsAsync()
        {
            try
            {
                var request = new RestRequest("api/symbols", DataFormat.Json);
                request.AddQueryParameter("access_key", this._token);
                request.AddQueryParameter("format", "1");
                var response = await _webClient.GetAsync<CurrencySymbolsResponse>(request);
                if(response.IsSuccess)
                {
                    var collection = new CurrencySymbolCollection();
                    foreach(var kv in response.Symbols)
                    {
                        collection.AddItem(new CurrencySymbol(kv.Key, kv.Value));
                    }
                    var result = new ApiResponse<ApiException, CurrencySymbolCollection>(null, collection);
                    return await Task.FromResult(result);
                }

                return await Task.FromResult(new ApiResponse<ApiException, CurrencySymbolCollection>(new ApiException(Constants.CurrencyService.FAILED_TO_FETCH_SYMBOLS, null), null));
            } 
            catch(Exception ex)
            {
                var result = new ApiResponse<ApiException, CurrencySymbolCollection>(new ApiException(Constants.CurrencyService.FAILED_TO_FETCH_SYMBOLS, null, ex), null);
                return await Task.FromResult(result);
            }
        }
    }
}
